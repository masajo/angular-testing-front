// Creamos nuestro primer SPEC de Cypress para que se pueda ejecutar


describe('Pruebas de ejemplo', () => {

  it('Comprobar Saludo', () => {
    cy.visit('/').then(() => {
      cy.get('#saludo').should('have.text', 'Hola, Martín');
    });
  });

  it('Otra manera de comprobar Saludo', () => {
    cy.visit('/').then(() => {
      cy.get('#saludo').should(($h1) => {
        expect($h1).to.have.text('Hola, Martín');
      });
    });
  });


  it('Seleccionar un alumno y ver sus detalles', () => {
    cy.visit('/');
    cy.get('#alumno-5').click().then(() => {
      cy.get('#detalle').should('contain.text', 'Charles Morris')
    });
  });

  it('Otra manera de seleccionar un alumno y ver sus detalles', () => {
    cy.visit('/');
    cy.get('#alumno-5').click().then(() => {
      cy.get('#detalle').should(($p) => {
        expect($p.text().trim()).to.equal('Charles Morris');
      });
    });
  });

  it('Escribir mensaje, mandar mensaje y mostrar alerta', () => {
    cy.visit('/');

    cy.get('#input-mensaje').focus().clear().type('¡HOLA!');

    const stub = cy.stub();
    cy.on ('window:alert', stub);

    cy
    .get('#boton-emitir').contains('Enviar Mensaje').click()
    .then(() => {
      expect(stub.getCall(0)).to.be.calledWith('¡HOLA!')
    })

  });

});
