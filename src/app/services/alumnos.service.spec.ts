import { getTestBed, TestBed } from '@angular/core/testing';
// Importamos los módulos de simulación de peticiones HTTP que trae Angular
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';


import { AlumnosService } from './alumnos.service';

/**
 *
 * AlumnosService es un servicio que realiza peticiones HTTP
 * para obtener una lista de alumnos a la url:
 * https://reqres.in/api/users
 *
 * Dado que las pruebas unitarias y de integración son
 * simulaciones de comportamiento para comprobar que el SUT
 * (System Under Tests), en nuestro caso el AlumnosService
 * funciona correctamente.
 *
 * Todas las dependencias que el SUT tenga, deben ser simuladas
 * Ya que lo que las pruebas del SUT deberían ser aisladas de errores
 * ajenos al SUT. Es decir, las pruebas de este servicio
 * no deben depender de si ReqRes.in tiene o no errores internamente.
 *
 * Es decir, debemos asegurar que AlumnosService funcione bien
 * teniendo en cuenta un buen funcionamiento de la dependencia que tenga.
 *
 * Los tests de AlumnosService no deberían fallar a consecuencia de errores
 * en la implementación de terceras partes o dependencias ajenas a esta clase.
 *
 * Esto garantiza que la clase AlumnosService esté bien planteada.
 *
 * A estas simulaciones se les llama Stubs o Mocks
 *
 * - Stub: Es un comportamiento hardcodeado.
 * -- Ejemplo: Risas enlatadas de los programas de la TV siempre suenan igual
 * -- Si yo llamo a un método stub, éste siempre devolverá lo mismo
 *
 * - Mock: Es un Stub personalizado para diferentes ocasiones.
 * Sirve para crear Stubs para diferentes ocasiones.
 *
 */


// Creamos una lista falsa / mock / stub de alumnos que serán los que se devuelvan al
// realizar peticiones http a https://reqres.in/api/users
const stubAlumnos = [
  {
    id: 1,
    email: "george.bluth@reqres.in",
    first_name: "George",
    last_name: "Bluth",
    avatar: "https://reqres.in/img/faces/1-image.jpg"
  },
  {
    id: 2,
    email: "janet.weaver@reqres.in",
    first_name: "Janet",
    last_name: "Weaver",
    avatar: "https://reqres.in/img/faces/2-image.jpg"
    },
    {
    id: 3,
    email: "emma.wong@reqres.in",
    first_name: "Emma",
    last_name: "Wong",
    avatar: "https://reqres.in/img/faces/3-image.jpg"
    },
    {
    id: 4,
    email: "eve.holt@reqres.in",
    first_name: "Eve",
    last_name: "Holt",
    avatar: "https://reqres.in/img/faces/4-image.jpg"
    },
    {
    id: 5,
    email: "charles.morris@reqres.in",
    first_name: "Charles",
    last_name: "Morris",
    avatar: "https://reqres.in/img/faces/5-image.jpg"
    },
    {
    id: 6,
    email: "tracey.ramos@reqres.in",
    first_name: "Tracey",
    last_name: "Ramos",
    avatar: "https://reqres.in/img/faces/6-image.jpg"
    }
]


/**
 * Cuando hagamos petición a https://reqres.in/api/users/{id}
 * Simulamos que nos devuelve un stub
*/
const stubAlumno = {
    id: 1,
    email: "george.bluth@reqres.in",
    first_name: "George",
    last_name: "Bluth",
    avatar: "https://reqres.in/img/faces/1-image.jpg"
}



describe('AlumnosService', () => {

  // Declaramos las variables que vamos a utilizar
  let testBed: TestBed;
  let httpMock: HttpTestingController;
  let service: AlumnosService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      // Tenemos que configurar el módulo para que funcione
      // imports y providers
      imports: [HttpClientTestingModule],
      providers: [AlumnosService]
    });

    // Inicializamos las variables
    testBed = getTestBed();
    httpMock = testBed.inject(HttpTestingController)
    service = testBed.inject(AlumnosService);

  });

  // * Después de cada test, verificamos HTTPMOCK
  afterEach(() => {
    httpMock.verify(); // Saber si se ha ejecutado o no el mock de http
  });

  // ? Test por defecto
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // * Test para obtener la lista de alumnos
  it('Debería devolver una lista de alumnos', () => {

    /**
     * TODO: El servicio debe tener un método llamado getAlumnos() que
     * devuelva un Observable de Alumno[]
     **/

    // * 1. Realizamos la llamada a getAlumnos
    service.getAlumnos().subscribe(
      (respuesta) => {
        // *4. El flush del httpMock nos emite el stubAlumnos si dentro del método getAlumnos()
        // * del servicio se ha llevado a cabo una petición a 'https://reqres.in/api/users'
        expect(respuesta).toBe(stubAlumnos);
      }
    );

    // * 2. Cuando en el servicio, se realiza una petición HTTP,
    // El httpMock simula la petición
    // Por lo que debemos comprobar que ha simulado UNA petición a
    // 'https://reqres.in/api/users'
    const peticion = httpMock.expectOne('https://reqres.in/api/users');

    // * 3. El httpMock emite la lista de Alumnos como STUB
    // Al hacer el FLUSH, la subscripción recibe la respuesta
    // Hasta que no se hace el flush, no se simula la devolución del Observable
    // de la línea 147 de este archivo
    peticion.flush(stubAlumnos);

    // * 5 Esperamos que la petición que ha simulado es de tipo GET
    // Es decir, que en el servicio se ha realizado un
    // * this._http.get('https://reqres.in/api/users')
    // en el método getAlumnos() del servicio
    expect(peticion.request.method).toBe('GET');

  });

  // * Test para obtener un Alumno
  // El servicio de Alumnos debe devolver un Alumno cuando se llame
  // a la ruta https://reqres.in/users/{id}
  it('Debería devolver un alumno si el ID existe', () => {

    let idAlumno: number = 1;

    service.getAlumno(idAlumno).subscribe(
      (respuesta) => {
        expect(respuesta).toBe(stubAlumno);
      }
    );

    const peticion = httpMock.expectOne(`https://reqres.in/api/users/${idAlumno}`);
    peticion.flush(stubAlumno);
    expect(peticion.request.method).toBe('GET');

  });

  // TODO: Obtener alumnos pasándole el número de página


  // TODO: Obtener un Alumno cuyo ID no existe



});
