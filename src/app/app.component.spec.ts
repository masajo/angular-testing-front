import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ListaAlumnosComponent } from './components/lista-alumnos/lista-alumnos.component';

describe('AppComponent', () => {

  // Declaramos las variables que vamos a usar
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;

  // Ejecutar líneas de código previas a cada uno de los IT
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ListaAlumnosComponent
      ],
      imports: [
        HttpClientModule,
        FormsModule
      ]
    }).compileComponents();

    // Inicializamos las variables fixture y app
    // Antes de cada test, para asegurarnos de que cada IT está aislado
    // y no depende de la ejecución de ningún otro IT. Esto garantiza
    // que podamos ejecutarlos en orden aleatorio o por separado.
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

  });

  it('should create the app', () => {
    // Comprobar que se puede instanciar un objeto AppComponent
    // Al ser un componente, usamos createComponent para crear ese objeto
    // que es lo mismo que en un HTML hagamos uso de su selector <app-root></app-root>
    expect(app).toBeTruthy(); // comprobar que la instancia existe
  });

  it(`should have as title 'angular-testing-front'`, () => {
    // Comprobamos que la propiedad title dentro de la clase app.component.ts
    // tiene un valor de "angular-testing-front"
    expect(app.title).toEqual('angular-testing-front');
  });

  // Shallow Testing: Pruebas sobre la vista del componente
  // Es decir, es una prueba que verifica algo del HTML
  it('should render title', () => {
    // Asegurarse de que la vista va a tener una estrategia de ChangeDetection de tipo Default
    // y ási los cambios en el ts, están siempre actualizados en el HTML
    // Cuando usamos el detectChanges() estamos forzando a comprobar los cambios que haya podido
    // haber en el HTML
    fixture.detectChanges();
    // A través de nativeElement, accedemos al elemento del DOM (La representación HTML del componente)
    // se convierte a HTMLElement para asegurar que podemos realizar búsquedas dentro del mismo.
    const compiled: HTMLElement = fixture.nativeElement as HTMLElement;
    // Esperamos que un elemento con clase="content" tenga dentro un "<span></span>"
    // Cuyo texto contenido sea 'angular-testing-front app is running!'
    expect(compiled.querySelector('.content span')?.textContent).toContain('angular-testing-front app is running!');
  });
});
