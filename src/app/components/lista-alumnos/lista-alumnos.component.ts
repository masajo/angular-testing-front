import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AlumnosService } from 'src/app/services/alumnos.service';

@Component({
  selector: 'app-lista-alumnos',
  templateUrl: './lista-alumnos.component.html',
  styleUrls: ['./lista-alumnos.component.scss']
})
export class ListaAlumnosComponent implements OnInit {

  // Lista de alumnos que se rellena con los datos que nos devuelve el servicio
  // TODO: crear tipo Alumno o Interface
  alumnos: any[] = [];
  alumnoSeleccionado = {
    id: 0,
    email: '',
    first_name: '',
    last_name: '',
    avatar: '',
  };
  mensajeNuevo: string = 'Mensaje para el padre'; // Mensaje que se envía con el EventEmitter
  @Input() usuario: string = '';
  @Output() mensaje: EventEmitter<string> = new EventEmitter<string>();


  constructor(public service: AlumnosService) { }

  ngOnInit(): void {

    this.service.getAlumnos().subscribe(
      (respuesta) => {
        // respuesta es el json con una clave data donde está la lista
        // por lo tanto accedemos a "data"
        this.alumnos = respuesta.data;

      },
      (error) => alert(`Ha ocurrido un error al obtener los alumnos: ${error}`),
      () => console.log('Lista de alumnos obtenida con éxito')
    )

  }

  // Método que se ejecuta cuando hacemos click en un alumno de la lista
  // Recibe el ID para saber cuál es el alumno seleccionado de la lista y así poder
  // guadarlo en la variable AlumnoSeleccionado
  seleccionarAlumno(id: number){

    this.service.getAlumno(id).subscribe(
      (respuesta) => {
        // Detro de respuesta está "data" y dentro los datos del alumno
        this.alumnoSeleccionado = respuesta.data;
      },
      (error) => alert(`Ha habido un error al obtener el detalle del alumno: ${error}`),
      () => console.log('Detalle del Alumno Seleccionado obtenido con éxito')
    );

  }

  // Método que se ejecuta cuando se pulsa el botón para Enviar Mensaje al padre
  mandarMensaje(){
    if (this.mensajeNuevo) {
      // Aquí hacemos el emit() del eventEmitter
      this.mensaje.emit(this.mensajeNuevo);
      this.mensajeNuevo = ''; // lo reseteamos para dejar en blanco el campo de input del html
    }
  }

}
