import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

// Imports propios
import { AlumnosService } from 'src/app/services/alumnos.service';
import { AlumnosServiceStub } from 'src/app/services/mocks/alumnos.service.mock';
import { ListaAlumnosComponent } from './lista-alumnos.component';


/**
 * Nuestro componente ListaAlumnosComponent depende directamente
 * del comportamiento correcto de AlumnosService.
 *
 * Depende de AlumnosService porque necesita la lista de Alumnos
 * que se la da AlumnosService para poder pintar en el HTML la lista de alumnos
 * y poder trabajar con ella en su TS.
 *
 * En el componente dentro de su constructor injectábamos una variable privada
 * del Servicio --> UNA DEPENDENCIA que tiene que ser simulada para que los tests
 * de nuestro ListaAlumnosComponent NO DEPENDA de que el SERVICIO llamado AlumnosService
 * esté bien/mal implementado.
 *
 * En este test nuestro SUT (System Under Test) es ListaAlumnosComponent,
 * por lo tanto toda dependencia debe ser MOCKEADA / SIMULADA
 */


describe('Pruebas para el componente ListaAlumnosComponent', () => {

  let fixture: ComponentFixture<ListaAlumnosComponent>;
  let component: ListaAlumnosComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListaAlumnosComponent],
      imports: [
        HttpClientModule,
        FormsModule
      ],
      providers: [
        // * IMPORTANTE:
        // * Le decimos, que donde el componente ListaALumnosComponent
        // * use AlumnosService, realmente use AlumnosServiceStub
        {
          provide: AlumnosService,
          useClass: AlumnosServiceStub
        }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAlumnosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {

    // toBeTrue != toBeTruthy
    // toBeTrue es que el valor tiene que ser true (boolean)
    // toBeFalse es que le valor tiene que ser false (boolean)
    // toBeTruthy es que tiene que ser true, !=0, '', !null, !undefined --> Que tenga valor válido
    // toBeFalsy es que tiene que ser false, 0, ', null o undefined --> Que no tenga valor válido
    expect(component).toBeTruthy(); //  aquí comprueba que no sea undefined
  });


  // * Se debería llamar a getAlumnos del servicio en el ngOnInit del componente
  // * para obtener todos los alumnos que nos devuelve el servicio
  it('Se debe llamar a getAlumnos del Servicio en ngOnInit', () => {

    // Ponemos un espía a contar las veces que se llama al método
    // getAlumnos del servicio que hay en el componente
    // Además, esta escucha está condicionada a que se llame y se ejecute hasta el final
    spyOn(component.service, 'getAlumnos').and.callThrough();

    // forzamos una llamada al método ngOnInit() del componente
    component.ngOnInit();

    // Verificamos que se haya llamado a _service.getAlumnos() una vez
    expect(component.service.getAlumnos).toHaveBeenCalledTimes(1);

  });


  // Test para comprobar que cuando el componente está estable (ha ejecutado el ngOnInit)
  // y está listo para poder acceder a todas sus propiedades, funciones, etc.
  // Se pueda verificar que tenemos la lista de alumnos en la propiedad adecuada
  it('Debería obtener del servicio la lista de contactos y guardarla', () => {

    fixture.whenStable().then(() => {
      // Cuando ya está estable, ya debería tener el listado de alumnos
      // Verificamos que no está vacía, es decir que su logitud es mayor que 0
      expect(component.alumnos.length).toBeGreaterThan(0);
    })

  });

  // SHALLOW Test para comprobar que el componente pinta correctamente en el HTML
  // la lista de alumnos obtenida del servicio
  it('Debería renderizar la lista de alumnos en el HTML', () => {
    fixture.whenStable().then(() => {
      // Nos aseguramos de que la vista se actualice
      fixture.detectChanges()
      // Obetenemos un elemento del DOM que va a tener una clase .alumnos
      // donde va a estar la lista de alumnos
      const elemento = fixture.debugElement.query(By.css('.alumnos')).nativeElement;
      // Esperamos que DENTRO de .alumnos haya al menos 1 nodo hijo (un elemento dentro)
      // <div class="alumnos">
      //    ... NODOS del elemento (childNodes que es un array)
      // </div>
      expect(elemento.childNodes[1]).not.toBeNull();
    })
  });

  // Test para comprobar que el componente listaAlumnos puede recibir
  // un @Input con el usuario que accede a la aplicación para mostrar
  // un saludo en el HTML
  it('Debería poder recibir un Usario como @Input y renderizar un saludo', () => {

    // Simulamos un paso de valor por @Input:
    // <app-lista-alumnos usuario='Martín'></app-lista-alumnos>
    component.usuario = 'Martín';
    fixture.detectChanges();
    // Otra manera de acceder a elementos nativos
    const elemento = fixture.debugElement.nativeElement;
    // Buscamos el elemento con id="saludo" y obtenemos su texto para verificar que
    // coincide con el valor del usuario pasado por @input y el saludo
    // <p id="saludo">Hola, {{usuario}}</p>
    expect(elemento.querySelector('#saludo').textContent.trim()).toBe('Hola, Martín')


  });

  // TODO: REVISAR para que nuevoMensaje tenga el valor
  // Test para comprobar que el componente puede emitir mensajes como @Output
  it('Debería emitir un mensaje a través de un evento @Output', async () => {

    // Creamos un espía del método "emit()" del EventEmitter<string> de tipo @Output
    // con nombre mensaje
    spyOn(component.mensaje, 'emit');
    // Obtenemos el botón que se encarga de emitir el mensaje
    // <button id="boton-emitir" (click)="mandarMensaje()">Enviar mensaje</button>
    const boton = fixture.nativeElement.querySelector('#boton-emitir');
    // Obtenemos el valor de el campo de texto donde se escribe el mensaje a enviar
    // y escribimos un mensaje "Mensaje para el padre"
    // <input id="input-mensaje" [(ngModel)]="mensajeNuevo" />
    fixture.nativeElement.querySelector('#input-mensaje').value = 'Mensaje para el padre';
    // Obtenemos el contenido del campo input después de haberle dado valor
    const nuevoMensaje = await fixture.nativeElement.querySelector('#input-mensaje').value;
    // Pulsamos el botón para emitir el mensaje al padre --> dentro ejecuta el emit()
    await boton.click();
    fixture.detectChanges();

    // Comprobamos que, en efecto, el método emit, ha sido ejecutado
    // implicando por tanto que el evento click del botón ejecuta un método en la clase
    // que ejecuta el método emit() del EventEmitter con "nuevoMensaje" como parámetro

    // Es decir, dentro del método mandarMensaje() se tiene que ejecutar:
    // this.mensaje.emit(mensajeNuevo)
    expect(component.mensaje.emit).toHaveBeenCalledWith(nuevoMensaje);

  });

  // Bloque de pruebas para cuando se pulsa sobre un alumno
  describe('Comportamientos del alumno seleccionado', () => {

    // Test para comprobar el estado del HTML antes de pulsar sobre ningún alumno de la lista
    it('Inicialmente, los detalles del alumno deben estar vacíos y se debe mostrar un mensaje', () => {

      // Tratamos de obtener el elemento que contiene el detalle del alumno seleccionado
      // a través del id="detalle"
      // Solo está presente cuando se pulse sobre un alumno, sino, no se debería poder encontrar
      const alumnoDetalle = fixture.debugElement.nativeElement.querySelector('#detalle');
      expect(alumnoDetalle).toBeNull();

      // Obtenemos el mensaje que indica que es necesario seleccionar un alumno
      const mensaje = fixture.debugElement.nativeElement.querySelector('#mensaje');
      // Obtenemos el contenido del mensaje
      // <p id="mensaje">
      //  Por favor, selecciona un alumno
      // </p>
      // Es lo mismo que:
      // <p innerHTML="Por favor, selecciona un alumno"></p>
      expect(mensaje.innerHTML.trim()).toBe('Por favor, selecciona un alumno');

    });

    // Test patra comprobar el comportamiento del HTML cuando se pulsa sobre un alumno
    it('Al hacer click sobre un alumno se muestrar los detalles del alumno', () => {

      // Nos aseguramos que tenemos la lista de los alumnos
      fixture.whenStable().then(() => {

        fixture.detectChanges();
        // Elegimos qué alumno va a recibir la pulsación
        const alumnoSeleccionado = fixture.debugElement.nativeElement.querySelector('#alumno-1')

        // Pulsamos sobre el alumno
        alumnoSeleccionado.click();

        fixture.whenStable().then(() => {

          fixture.detectChanges();

          // Obtememos el elemento con los detalles del alumno
          const alumnoDetalle = fixture.debugElement.nativeElement.querySelector('#detalle');
          // Comprobamos que la propiedad alumnoSeleccionado tiene ahora el nombre de George
          // en su clave first_name
          expect(component.alumnoSeleccionado.first_name).toBe('George');
          // Comprobamos que el detalle del alumno contiene el nombre + apellido
          expect(alumnoDetalle.innerHTML.trim()).toBe('George Bluth')

        });

      });

    });

  });

});



// * EJEMPLOS DE MATCHERS *********
// Creamos un describe a parte para probar los matchers y practicar
// ? Comentados con una X delante de scribe para que no se ejecuten
// ? Si quieres ejecutarlos, quita la 'x' de delante de describe
xdescribe('Pruebas de Matchers para coger confianza', () => {

  let logged: boolean;
  let nombre: string;
  let listaCompra: string[];
  let persona: {nombre:string, email: string, edad: number};

  beforeEach(() => {
    // Antes de cada test, se inicializan las variables
    // para garantizar que cada test empieza con los mismos valores
    logged = false;
    nombre = 'Martín';
    listaCompra = ['patatas', 'leche'];
    persona = {
      nombre: 'Pepe',
      email: 'pepe@imaginagroup.com',
      edad: 29
    };
  })

  it('To Be', () => {
    expect(logged).toBe(false);
    expect(nombre).toBe('Martín');
  });

  // NOT se puede concatenar para hacer la comparación inversa
  // se puede concatenar con cualquier Matcher de Jasmine
  it('Not To Be', () => {
    expect(listaCompra).not.toBe(['lechuga', 'pollo']);
    expect(logged).not.toBe(true);
  });

  it('Undefined y Null', () => {
    expect(persona).not.toBe({nombre: '', email: '', edad: 0}); // que no esté vacío
    expect(persona).not.toBeNull(); // que no sea null
    expect(persona).not.toBeUndefined(); // que no sea undefined
    expect(persona).toBeDefined(); // que sea defined (no sea undefined)
  });


  it('To Contain', () => {
    expect(nombre).toContain('M');
    expect(listaCompra).toContain('patatas');
  });

  // En este test no se espera nada concreto
  it('Nothing', () => {
    // realizas determinadas operaciones que se deben ejecutar
    // pero al final no hay nada concreto que comprobar
    // Por ejemplo, en una función que ejecuta algo, pero no devuelve nada (void)
    // Solo se usa en funciones/métodos que no tienes capacidad concreta de asegurar
    // una comparación que verifique que test pasa.
    // Es una manera explícita de decir que no hay nada que comprobar
    expect().nothing();
  });

  it('To Be Close To', () => {
    expect(persona.edad).toBeCloseTo(29, 0)
  });

  it('Greater/Smaller', () => {


    expect(persona.edad).toBeGreaterThan(28);
    expect(persona.edad).toBeGreaterThanOrEqual(29);
    expect(persona.edad).toBeLessThan(30);
    expect(persona.edad).toBeLessThanOrEqual(29);

    expect(listaCompra.length).toBeGreaterThanOrEqual(2);
    expect(nombre.length).toBeLessThanOrEqual(6);

  });

  it('Instance Of', () => {
    expect(nombre).toBeInstanceOf(String);
    expect(persona.edad).toBeInstanceOf(Number);
  });


});


